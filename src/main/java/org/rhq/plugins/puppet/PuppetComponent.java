package org.rhq.plugins.puppet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.rhq.core.domain.configuration.Configuration;
import org.rhq.core.pluginapi.inventory.CreateChildResourceFacet;
import org.rhq.core.pluginapi.inventory.CreateResourceReport;
import org.rhq.core.pluginapi.inventory.DeleteResourceFacet;
import org.rhq.core.pluginapi.inventory.InvalidPluginConfigurationException;
import org.rhq.core.pluginapi.inventory.ResourceComponent;
import org.rhq.core.pluginapi.operation.OperationFacet;
import org.rhq.core.pluginapi.operation.OperationResult;
import org.rhq.core.system.ProcessExecutionResults;
import org.rhq.core.system.SystemInfo;
import org.rhq.plugins.script.ScriptServerComponent;

public class PuppetComponent extends ScriptServerComponent implements ResourceComponent, OperationFacet, CreateChildResourceFacet,
    DeleteResourceFacet {
    
    private final Log log = LogFactory.getLog(this.getClass());

    // these are reused from our generic script plugin
    protected static final String PLUGINCONFIG_EXECUTABLE = ScriptServerComponent.PLUGINCONFIG_EXECUTABLE;

    public OperationResult invokeOperation(String name, Configuration params) throws Exception {

        // TODO
        OperationResult res = new OperationResult();
        if ("dummyOperation".equals(name)) {
        }
        return res;
    }

    public CreateResourceReport createResource(CreateResourceReport report) {
        return null; // TODO change this
    }

    public void deleteResource() throws Exception {
        // TODO supply code to delete a child resource
    }

    protected static ProcessExecutionResults executeExecutable(SystemInfo sysInfo, Configuration pluginConfig,
        String args, long wait, boolean captureOutput) throws InvalidPluginConfigurationException {
        return ScriptServerComponent.executeExecutable(sysInfo, pluginConfig, args, wait, captureOutput);
    }
}
