package org.rhq.plugins.puppet;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.rhq.core.domain.configuration.Configuration;
import org.rhq.core.domain.configuration.PropertySimple;
import org.rhq.core.domain.resource.ResourceType;
import org.rhq.core.pluginapi.inventory.DiscoveredResourceDetails;
import org.rhq.core.pluginapi.inventory.ResourceDiscoveryComponent;
import org.rhq.core.pluginapi.inventory.ResourceDiscoveryContext;
import org.rhq.core.system.ProcessExecution;
import org.rhq.core.system.ProcessExecutionResults;
import org.rhq.core.system.SystemInfo;
import org.rhq.plugins.script.ScriptDiscoveryComponent;

/**
 * Puppet client discovery class.
 */
public class PuppetDiscovery extends ScriptDiscoveryComponent implements ResourceDiscoveryComponent {

    private final Log log = LogFactory.getLog(this.getClass());

    @Override
    public Set<DiscoveredResourceDetails> discoverResources(ResourceDiscoveryContext discoveryContext) {

        String executable = checkExecutables(discoveryContext.getSystemInformation());

        // if null, it means puppet is not installed or we could not auto-detect its location
        if (executable == null) {
            return new HashSet<DiscoveredResourceDetails>();
        }

        // create our plugin config with the executable path, and use the manual-add facet to create our details
        Configuration pluginConfig = discoveryContext.getDefaultPluginConfiguration();
        pluginConfig.put(new PropertySimple(PuppetComponent.PLUGINCONFIG_EXECUTABLE, executable));

        Set<DiscoveredResourceDetails> discoveredResources = new HashSet<DiscoveredResourceDetails>();
        DiscoveredResourceDetails details = discoverResource(pluginConfig, discoveryContext);
        if (details != null) {
            discoveredResources.add(details);
        }

        return discoveredResources;
    }

    @Override
    public DiscoveredResourceDetails discoverResource(Configuration config, ResourceDiscoveryContext context) {
        DiscoveredResourceDetails details = super.discoverResource(config, context);
        if (details != null) {
            details.setResourceName("Puppet Client");
            log.info("Discovered new puppet client at ["
                + config.getSimpleValue(PuppetComponent.PLUGINCONFIG_EXECUTABLE, "?") + "]");
        }
        return details;
    }

    @Override
    protected String determineDescription(ResourceDiscoveryContext context, Configuration pluginConfig) {
        return "Puppet client that applies recipes to the system";
    }

    private String checkExecutables(SystemInfo systemInfo) {
        // Make sure puppet is actually on the system
        // First we see if its in a typical place.
        String[] executables = new String[] { "/usr/bin/puppet", "/bin/puppet" };
        for (String executable : executables) {
            File file = new File(executable);
            if (file.canRead()) {
                return executable;
            }
        }

        // We can't find it, use 'which' to see if it knows
        String executable = null;

        try {
            ProcessExecution processExecution = new ProcessExecution("which");
            processExecution.setArguments(new String[] { "puppet" });
            processExecution.setCaptureOutput(true);

            ProcessExecutionResults executionResults = systemInfo.executeProcess(processExecution);
            String capturedOutput = executionResults.getCapturedOutput();
            executable = (((capturedOutput == null) || "".equals(capturedOutput)) ? null : capturedOutput.trim());
        } catch (Exception e) {
            log.debug("'which' failed to determine where the puppet executable is. Cause: " + e);
        }

        return executable;
    }
}